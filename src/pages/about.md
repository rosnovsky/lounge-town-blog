---
layout: ../layouts/AboutLayout.astro
title: "About"
---

Hi, I'm [Art](https://rosnovsky.us), a software engineer, a hiker, and an avid reader from Western Washington.

My first Mastodon instance was a single-user one that I set up in 2019 and I ran on my old Dell server out of my garage. It was a fun and enlightening experience, and I fell in love with Fediverse. Alas, long story short, running a single-user instance is not something that was sustainable for me personally, and my old server eventually gave out. I could've restored it but made a few mistakes and the instance was lost forever.

[Lounge Town](https://lounge.town) is in part a return to those happier times, and in part a response to what happened to Twitter.

I've learned a lot from my venturing into the Fediverse back in 2019. Both from the technical and infrastructure perspective, and from the human side of things. It's always better to have folks around, and here you have, Lounge Town.

With time, I'll post all the juicy details about how I run this instance, and how fulfilling it sometimes is.

I hope you like it here!
