import type { Site, SocialObjects } from "./types";

export const SITE: Site = {
  website: "https://mayor.lounge.town/",
  author: "Art Rosnovsky",
  desc: "Official Communications from the Mayor of Lounge Town",
  title: "Lounge Town Mayor",
  ogImage: "astropaper-og.jpg",
  lightAndDarkMode: true,
  postPerPage: 5,
};

export const LOGO_IMAGE = {
  enable: false,
  svg: true,
  width: 216,
  height: 46,
};

export const SOCIALS: SocialObjects = [
  {
    name: "Mastodon",
    href: "https://lounge.town/@rosnovsky",
    linkTitle: `${SITE.title} on Mastodon`,
    active: true,
  },
  {
    name: "Mail",
    href: "mailto:mayor@lounge.town",
    linkTitle: `Send an email to ${SITE.title}`,
    active: true,
  },
  {
    name: "GitLab",
    href: "https://gitlab.com/rosnovsky/lounge-town-blog",
    linkTitle: `${SITE.title} on GitLab`,
    active: true,
  },
];
